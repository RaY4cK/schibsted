import os
import unittest
import itertools
from challenge.external_merge_sort import ExternalMergeSort


class TestExternalMergeSort(unittest.TestCase):

    def test_sort_list_with_integers(self):
        external_sorting = ExternalMergeSort(100*1024*1024)  # In bytes
        external_sorting.sort('./tests/data/test_file.txt', './tests/data/result.txt')
        with open('./tests/data/result.txt') as ordered_file, open('./tests/data/expected_result.txt') as result_file:
            for line1, line2 in itertools.izip(ordered_file, result_file):
                self.assertEqual(line1, line2)
        os.remove('./tests/data/result.txt')
