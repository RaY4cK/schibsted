import unittest
from challenge.quicksort import Quicksort


class TestQuicksort(unittest.TestCase):

    def test_sort_list_with_integers(self):
        list_to_sort = [1, 5, 4, 7, 2, 9, 3]
        correct_list = [1, 2, 3, 4, 5, 7, 9]
        ordered_list = Quicksort().sort(list_to_sort)

        for i in range(len(correct_list)):
            self.assertEqual(correct_list[i], ordered_list[i])

    def test_sort_list_with_strings(self):
        list_to_sort = ['bsdf bsdf', 'csdf asdf', 'asdf asdf', 'asdf bsdf', 'csdf bsdf', 'bsdf asdf']
        correct_list = ['asdf asdf', 'asdf bsdf', 'bsdf asdf', 'bsdf bsdf', 'csdf asdf', 'csdf bsdf']
        ordered_list = Quicksort().sort(list_to_sort)
        for i in range(len(correct_list)):
            self.assertEqual(correct_list[i], ordered_list[i])
