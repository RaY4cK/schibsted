#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: $0 NUMBER_OF_LINES FILE_NAME"
    exit 1
fi

ruby -e 'a=STDIN.readlines;'$1'.times do;b=[];16.times do; b << a[rand(a.size)].chomp end; puts b.join(" "); end' < /usr/share/dict/words > $2
