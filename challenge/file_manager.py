class FileManager:

    def __init__(self, tmp_files):
        # This list of files are the ones created in the split process
        self.tmp_files = tmp_files

        # A dict containing:
        #   key   = filename
        #   value = file descriptor to read from file
        self.files_data = None

    def read_initial_data(self, input_buffer):
        self.files_data = {f: open(f) for f in self.tmp_files}
        data = []
        for f in self.files_data:
            data.append((f, self.files_data[f].readlines(int(input_buffer))))
        return data

    def ask_for_data(self, filename, input_buffer):
        new_lines = self.files_data[filename].readlines(int(input_buffer))
        if not new_lines:
            self.close_file(filename)
        return new_lines

    def close_file(self, filename):
        self.files_data[filename].close()

    @staticmethod
    def read_lines(file_path, buffer_size):
        with open(file_path) as f:
            while True:
                lines = f.readlines(buffer_size)
                if not lines:
                    break
                yield lines

    @staticmethod
    def save_lines_in_file(lines, filename):
        with open(filename, 'w') as chunk_file:
            for line in lines:
                chunk_file.write("%s" % line)
