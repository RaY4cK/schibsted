class Quicksort:
    def sort_list_comprehension(self, list_to_sort):
        if not list_to_sort:
            return []
        pivot = list_to_sort[0]
        l = self.sort([x for x in list_to_sort[1:] if x < pivot])
        u = self.sort([x for x in list_to_sort[1:] if x >= pivot])

        return l + [pivot] + u

    def sort(self, list_to_sort):
        self.qsort_swap(list_to_sort, 0, len(list_to_sort))
        return list_to_sort

    def qsort_swap(self, list_to_sort, begin, end):
        if (end - begin) > 1:
            pivot = list_to_sort[begin]
            l = begin + 1
            r = end
            while l < r:
                if list_to_sort[l] <= pivot:
                    l += 1
                else:
                    r -= 1
                    list_to_sort[l], list_to_sort[r] = list_to_sort[r], list_to_sort[l]

            l -= 1
            list_to_sort[begin], list_to_sort[l] = list_to_sort[l], list_to_sort[begin]
            # print begin, end, list_to_sort
            self.qsort_swap(list_to_sort, begin, l)
            self.qsort_swap(list_to_sort, r, end)
