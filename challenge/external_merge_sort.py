from challenge.quicksort import Quicksort
from challenge.file_manager import FileManager
import os


class ExternalMergeSort:

    def __init__(self, memory_limit):
        self.memory_limit = memory_limit

    def sort(self, file_path, output_file):
        temporary_files = self.split_file(file_path)
        self.merge_files(temporary_files, output_file)

    def split_file(self, file_path):
        files = []
        file_id = 0
        for lines in FileManager.read_lines(file_path, self.memory_limit):
            Quicksort().sort(lines)
            filename = str(file_id) + '.txt'
            FileManager.save_lines_in_file(lines, filename)
            files.append(filename)
            file_id += 1
        return files

    def merge_files(self, files, result_file):
        manager = FileManager(files)
        input_buffer = self.memory_limit / len(files)
        data = manager.read_initial_data(input_buffer)

        output = open(result_file, 'w')

        while data:
            empty_filename, index = self.check_list_empty(data)
            if empty_filename:
                new_lines = manager.ask_for_data(empty_filename, input_buffer)
                if not new_lines:
                    # We have reached the end of file
                    data.pop(index)
                else:
                    data[index] = (empty_filename, new_lines)
            # If there are no more data to process,
            if not data:
                break
            # output min value and remove it from the list
            values = [d[0] for _, d in data]
            min_value_index = values.index(min(values))
            output.write(data[min_value_index][1].pop(0))

        output.close()

        # remove temporary files
        for tmp_file in files:
            os.remove(tmp_file)

    @staticmethod
    def check_list_empty(data):
        index = 0
        for file, item in data:
            if not item:
                return file, index
            index += 1
        return None, None
