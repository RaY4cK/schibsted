For more information about the challenge, please, read the doc/document.pdf

Requirements
------------
- python 2.7

How to run
----------
Unit tests:
    python -m unittest discover
Application:
    python -f "input_file_path" -o "output_file_path" -m "memory_size_in_bytes"

How to generate a data file
---------------------------
Run the script create_file.sh located in the bin folder:
    ./create_file.sh NUMBER_OF_LINES FILE_NAME