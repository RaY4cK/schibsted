from challenge.external_merge_sort import ExternalMergeSort
from optparse import OptionParser
import sys


def parse_options():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage=usage)

    parser.add_option("-f", "--file", dest="file_path", type="string",
                      help="File to sort", metavar="FILE")
    parser.add_option("-o", "--output", dest="output_file", type="string",
                      help="File to store the ordered list", metavar="FILE")
    parser.add_option("-m", "--memory-limit", dest="memory_limit",
                      type="int", help="Memory limit in bytes")

    if len(sys.argv[1:]) != 6:
        parser.print_help()
        exit(1)

    return parser.parse_args()


def main():
    (options, args) = parse_options()
    external_merge_sort = ExternalMergeSort(options.memory_limit)
    external_merge_sort.sort(options.file_path, options.output_file)

if __name__ == '__main__':
    main()
